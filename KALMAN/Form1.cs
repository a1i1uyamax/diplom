﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A1i1uyamax;
using A1i1uyamax.KA;
using A1i1uyamax.Statistics;

namespace KALMAN
{
    public partial class Form1 : Form
    {
        ExtendedKalmanFilter kalman;
        KAMatrix X, Z, Xreal;
        KAVector Ireal;
        Random rand;
        double dQ = 0.3, dW = 0.2, dI = 4.0, dt = 0.075, t;
        public Form1()
        {
            InitializeComponent();

            KAQuaternion Qreal = new KAQuaternion();
            Ireal = new KAVector(350, 50, 330);
            KAVector Wreal = new KAVector(0.25, 0.0, -0.25);
            t = 0.0;

            X = new KAMatrix(7, 1);
            Xreal = new KAMatrix(7, 1);
            Z = new KAMatrix(3, 1);

            Xreal[0, 0] = Qreal.scalar;
            Xreal[1, 0] = Qreal.vector.X;
            Xreal[2, 0] = Qreal.vector.Y;
            Xreal[3, 0] = Qreal.vector.Z;
            Xreal[4, 0] = Wreal.X;
            Xreal[5, 0] = Wreal.Y;
            Xreal[6, 0] = Wreal.Z;
            
            //////////////
            //Xreal[7, 0] = Ireal.X;
            //Xreal[8, 0] = Ireal.Y;
            //Xreal[9, 0] = Ireal.Z;
            //////////////

            for (int i = 0; i < Xreal.Row; i++) X[i, 0] = Xreal[i, 0];
            rand = new Random(DateTime.Now.Millisecond);

            ///////////////
            //for (int i = 7; i < 10; i++) X[i, 0] += 2.0 * rand.NextDouble() * dI - dI;
            ///////////////

            KAMatrix P0 = new KAMatrix(X.Row, X.Row);
            KAMatrix R = new KAMatrix(Z.Row, Z.Row);

            for (int i = 0; i < 3; i++) R[i, i] = dW * dW;//dQ * dQ;
            //for (int i = 4; i < 7; i++) R[i, i] = dW * dW;

            for (int i = 0; i < 4; i++) P0[i, i] = 0.7;//dQ * dQ;
            for (int i = 4; i < 7; i++) P0[i, i] = 0.5;//dW * dW;
            //for (int i = 7; i < 10; i++) P0[i, i] = 0.9;//dI * dI;

            kalman = new ExtendedKalmanFilter(X, P0, R, Z.Row, dt, f, h);

            for (int i = 0; i < Z.Row; i++) Z[i, 0] = Xreal[i + 4, 0];

            Charting();
        }
        KAMatrix f(KAMatrix X, double dt)
        {
            KAQuaternion Q = new KAQuaternion(X[1, 0], X[2, 0], X[3, 0], X[0, 0]);
            KAVector W = new KAVector(X[4, 0], X[5, 0], X[6, 0]);
            //KAVector Ie = new KAVector(X[7, 0], X[8, 0], X[9, 0]);

            W = Numerical.RK4(Ireal, KAVector.NULL, W, dt);
            Q = Numerical.RK4(W, Q, dt);

            KAMatrix M = new KAMatrix(7, 1);
            M[0, 0] = Q.scalar;
            M[1, 0] = Q.vector.X;
            M[2, 0] = Q.vector.Y;
            M[3, 0] = Q.vector.Z;
            M[4, 0] = W.X;
            M[5, 0] = W.Y;
            M[6, 0] = W.Z;
            //M[7, 0] = Ie.X;
            //M[8, 0] = Ie.Y;
            //M[9, 0] = Ie.Z;

            return M;
        }
        KAMatrix h(KAMatrix X, double dt)
        {
            KAMatrix M = new KAMatrix(7, 1);
            M = f(X, dt);
            KAMatrix H = new KAMatrix(3, 1);
            for (int i = 0; i < 3; i++) H[i, 0] = M[i + 4, 0];
            return H;
        }
        void DoZ()
        {
            double n;
            for (int i = 0; i < 3; i++)
            {
                n = 0;
                for (int j = 0; j < 20; j++) n += 2.0 * rand.NextDouble() * dW - dW;
                Z[i, 0] = Xreal[i + 4, 0] + n / 20.0;
            }
            //for (int i = 4; i < 7; i++)
            //{
            //    n = 0;
            //    for (int j = 0; j < 20; j++) n += 2.0 * rand.NextDouble() * dQ - dQ;
            //    Z[i, 0] = Xreal[i, 0] + n / 20.0;
            //}
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Xreal = f(Xreal, dt);
            DoZ();
            X = kalman.Step(Z);
            t += dt;

            Charting();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled ^= true;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            Application.Restart();
        }
        void Charting(System.Windows.Forms.DataVisualization.Charting.Chart chart, KAMatrix X, int a)
        {
            for (int i = 0; i < 3; i++) chart.Series[i].Points.AddXY(t, X[i + a, 0]); // a = 4
        }
        void Charting(System.Windows.Forms.DataVisualization.Charting.Chart chart, KAMatrix X)
        {
            for (int i = 0; i < 4; i++) chart.Series[i].Points.AddXY(t, X[i, 0]);
        }
        void Charting()
        {
            Charting(chart1, Xreal, 4);
            Charting(chart2, Z, 0);
            Charting(chart3, X, 4);

            Charting(chart4, Xreal);
            //Charting(chart5, Z);
            Charting(chart6, X);

            TextingBoxing();
        }
        void TextingBoxing()
        {
            KAMatrix XX = new KAMatrix(X.Row, 1);
            XX = X - Xreal;
            for (int i = 0; i < XX.Row; i++) XX[i, 0] = Math.Abs(XX[i, 0]);

            richTextBox1.Text += string.Format("{0:0.000}", t) + " c.:\n\nW:\t" + XX[0, 0].ToString()
                + "\nX:\t" + XX[1, 0].ToString() + "\nY:\t" + XX[2, 0].ToString() + "\nZ:\t"
                + XX[3, 0].ToString() + "\n\n\n";
            richTextBox2.Text += string.Format("{0:0.000}", t) + " c.:\n\nX:\t" + XX[4, 0].ToString()
                + "\nY:\t" + XX[5, 0].ToString() + "\nZ:\t" + XX[6, 0].ToString() + "\n\n\n";
            //richTextBox3.Text += string.Format("{0:0.000}", t) + " c.:\n\nX:\t" + XX[7, 0].ToString()
            //    + "\nY:\t" + XX[8, 0].ToString() + "\nZ:\t" + XX[9, 0].ToString() + "\n\n\n";
        }
    }
}
